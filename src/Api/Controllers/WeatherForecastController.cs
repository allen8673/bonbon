﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using AspnetApp.Models;
using AspnetApp.Services;
using AspnetApp.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace AspnetApp.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly IWeatherForecastService _service;
        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IWeatherForecastService service)
        {
            _logger = logger;
            _service = service;
        }

        [HttpGet("identity/{id}")]
        public virtual ActionResult<ResponseDetail> Get(int id)
        {
            return _service.Get(id);
        }


        [HttpPost]
        [TypeFilter(typeof(UnitOfWorkFilter))]
        public virtual ActionResult<ResponseDetail> Create(WeatherForecast weatherForecast)
        {
            return _service.Create(weatherForecast);
        }

        [HttpPatch("{id}")]
        [TypeFilter(typeof(UnitOfWorkFilter))]
        public virtual ActionResult<ResponseDetail> Update(int id, WeatherForecast weatherForecast)
        {
            weatherForecast.Id = id;
            return _service.Update(weatherForecast);
        }

        [HttpDelete("Delete")]
        [TypeFilter(typeof(UnitOfWorkFilter))]
        public virtual ActionResult<ResponseDetail> Delete(int id)
        {
            return _service.Delete(id);
        }

        // [Authorize(Roles = "admin")]
        [Authorize]
        [HttpGet]
        public virtual ActionResult<ResponseDetail> GetListPaged()
        {
            return _service.GetListPaged();
        }

        [HttpPost("TestError")]
        [TypeFilter(typeof(UnitOfWorkFilter))]
        public virtual ActionResult<ResponseDetail> TestError()
        {
            throw new NotImplementedException();
        }
    }
}
