using System;
using System.Reflection;
using DbUp;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace AspnetApp
{
    public class DatabaseInitFilter : IStartupFilter
    {

        private string connectionString;
        public DatabaseInitFilter(IConfiguration configuration)
        {
            connectionString = configuration.GetConnectionString("Postgres");
        }

        public Action<IApplicationBuilder> Configure(Action<IApplicationBuilder> next)
        {

            // "Host=localhost;User ID=postgres;Password=postgres@1234;Port=5432;Database=bonbon;";
            // "User ID=postgres;Password=postgres12345;Host=35.227.170.24;Port=5432;Database=bonbon;"

            EnsureDatabase.For.PostgresqlDatabase(connectionString); //Creates database if not exist

            var upgradeEngineBuilder = DeployChanges.To
                            .PostgresqlDatabase(connectionString)
                            .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly())
                            .WithTransaction()
                            .LogToConsole();

            var upgrader = upgradeEngineBuilder.Build();
            if (upgrader.IsUpgradeRequired())
            {
                var result = upgrader.PerformUpgrade();
            }

            return next;
        }
    }
}