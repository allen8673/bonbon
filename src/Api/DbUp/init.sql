CREATE TABLE IF NOT EXISTS weatherforecast (
	id serial PRIMARY KEY,
	date TIMESTAMP,
	temperature_c int NULL DEFAULT NULL,
	summary VARCHAR ( 50 ) NULL DEFAULT NULL
	-- username VARCHAR ( 50 ) UNIQUE NOT NULL,
	-- password VARCHAR ( 50 ) NOT NULL,
	-- email VARCHAR ( 255 ) UNIQUE NOT NULL,
    -- last_login TIMESTAMP 
);
