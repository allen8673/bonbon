using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AspnetApp.Models
{
    [Table("weatherforecast")]
    public class WeatherForecast
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("date")]
        public DateTime Date { get; set; }

        [Column("temperature_c")]
        public int TemperatureC { get; set; }

        [Editable(false)]
        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        [Column("summary")]
        [StringLength(50)]
        public string Summary { get; set; }
    }
}
