using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using AspnetApp.Services;
using AspnetApp.Utils;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using System.Linq;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using NSwag.AspNetCore;
using NSwag;
using System.Collections.Generic;
using NSwag.Generation.Processors.Security;
using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;

namespace AspnetApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // DbUp
            services.AddSingleton<IStartupFilter, DatabaseInitFilter>();

            // Controllers
            services.AddControllers(options =>
                {
                    options.Filters.Add<ResponseFilter>();
                })
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.SuppressModelStateInvalidFilter = true;
                });

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            // Services
            services.Scan(scan => scan
                .FromCallingAssembly()
                .AddClasses(classes => classes.AssignableTo(typeof(BaseService)))
                .AsMatchingInterface()
                .WithTransientLifetime());

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(options =>
            {
                options.RootPath = "ClientApp/build";
            });

            services.AddSwaggerDocument(options =>
            {
                options.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "ToDo API";
                    document.Info.Description = "A simple ASP.NET Core web API";
                    document.Info.TermsOfService = "None";
                    document.Info.Contact = new NSwag.OpenApiContact
                    {
                        Name = "Shayne Boyer",
                        Email = string.Empty,
                        Url = "https://twitter.com/spboyer"
                    };
                    document.Info.License = new NSwag.OpenApiLicense
                    {
                        Name = "Use under LICX",
                        Url = "https://example.com/license"
                    };
                };

                options.AddSecurity("oauth2", new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.OAuth2,
                    Flows = new OpenApiOAuthFlows
                    {
                        // ClientCredentials = new OpenApiOAuthFlow
                        // {
                        //     // AuthorizationUrl = "https://localhost:6001/connect/authorize",
                        //     TokenUrl = "https://localhost:6001/connect/token",
                        //     Scopes = new Dictionary<string, string> { { "api1", "My API" } }
                        // },

                        Password = new OpenApiOAuthFlow
                        {
                            TokenUrl = "https://localhost:6001/connect/token",
                            Scopes = new Dictionary<string, string> {
                                { "api1", "My API" } }
                        }
                    }
                });

                options.OperationProcessors.Add(new OperationSecurityScopeProcessor("oauth2"));
            });

            // JwtSecurityTokenHandler.InboundClaimTypeMap.Clear();


            services.AddAuthentication("Bearer")
            .AddJwtBearer("Bearer", options =>
            {
                options.Authority = "https://localhost:6001";
                // options.RequireHttpsMetadata = false;
                options.Audience = "MyBackendApi2";
                options.Events = new JwtBearerEvents()
                {
                    OnAuthenticationFailed = (e) =>
                    {
                        // this.logger.LogError(e.Exception.Message);
                        return Task.CompletedTask;
                    }
                };
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateAudience = false
                    // NameClaimType = "name",
                    // RoleClaimType = "role"
                };
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy("ApiScope", policy =>
                {
                    policy.RequireAuthenticatedUser();
                    policy.RequireClaim("scope", "api1");
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Global Exception
            app.UseExceptionHandler(err => err.UseCustomError(env));

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                // endpoints.MapControllerRoute(
                //     name: "default",
                //     pattern: "{controller}/{action=Index}/{id?}");
                endpoints.MapControllers();
            });

            // Swagger
            if (env.IsDevelopment())
            {
                app.UseOpenApi();
                app.UseSwaggerUi3(options =>
                {
                    // options.Path = "/api/swagger";
                    options.OperationsSorter = "method";

                    options.OAuth2Client = new OAuth2ClientSettings
                    {
                        ClientId = "password.client",
                        ClientSecret = "secret",
                        AppName = "Demo API - Swagger",
                        UsePkceWithAuthorizationCodeGrant = true
                    };
                });
            }

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    // spa.UseReactDevelopmentServer(npmScript: "start");
                    spa.UseProxyToSpaDevelopmentServer("http://localhost:3000");
                }
            });
        }
    }
}
