using System.Text.Json;
using Microsoft.AspNetCore.Mvc;

namespace AspnetApp.Utils
{
    public class ResponseDetail
    {
        public bool Success { get; private set; }
        public string Message { get; private set; }
        public object Data { get; private set; }

        // public string ToJson()
        // {
        //     return JsonSerializer.Serialize(this, new JsonSerializerOptions
        //     {
        //         PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        //     });
        // }
        public ResponseDetail(object data = null, string message = null, bool success = true)
        {
            Success = success;
            Message = message;
            Data = data;
        }
    }

}