using System;
using System.Data;

namespace AspnetApp.Utils
{
    public interface IUnitOfWork : IDisposable
    {
        IDbTransaction Transaction { get; }
        IDbConnection Connection { get; }
        void BeginTransaction();
        void Commit();
        void Rollback();
    }
}