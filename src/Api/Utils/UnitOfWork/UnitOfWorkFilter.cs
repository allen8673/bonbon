using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace AspnetApp.Utils
{
    public class UnitOfWorkFilter : IActionFilter
    {
        private ILogger<UnitOfWorkFilter> _logger;

        private IUnitOfWork _unitOfWork;

        public UnitOfWorkFilter(ILogger<UnitOfWorkFilter> logger, IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            _unitOfWork.BeginTransaction();
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            if (context.Exception == null)
            {
                _unitOfWork.Commit();
            }
            else
            {
                _unitOfWork.Rollback();
            }
        }
    }
}