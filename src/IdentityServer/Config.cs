﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Services;
using IdentityServer4.Test;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Email(),
                new IdentityResources.Profile(),
                new IdentityResources.Phone(),
                new IdentityResources.Address()
            };


        public static IEnumerable<ApiResource> ApiResources =>
             new ApiResource[]
            {
                new ApiResource("api1", "My API"),
                new ApiResource("MyBackendApi2", "My Backend API 2", new List<string>(){ ClaimTypes.Role }),
            };


        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            {
                new ApiScope("api1", "My API"),
            };

        public static IEnumerable<Client> Clients =>
            new List<Client>
            {
                new Client
                {
                    ClientId = "client",

                    // no interactive user, use the clientid/secret for authentication
                    AllowedGrantTypes = GrantTypes.ClientCredentials,

                    // secret for authentication
                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },

                    // scopes that client has access to
                    AllowedScopes = { "api1" },

                    AllowedCorsOrigins = {"https://localhost:5001"},
                },

                // new Client
                // {
                //     ClientId = "password.client",
                //     AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                //     ClientSecrets =
                //     {
                //         new Secret("secret".Sha256())
                //     },
                //     AllowedScopes = {
                //         "roles",
                //         IdentityServerConstants.StandardScopes.OpenId,
                //     },
                //     AlwaysSendClientClaims=true,
                //     ClientClaimsPrefix = string.Empty,
                //     AllowOfflineAccess = true,
                //     RefreshTokenUsage = TokenUsage.OneTimeOnly,
                //     AccessTokenLifetime = 18000,
                //     RefreshTokenExpiration = TokenExpiration.Absolute,
                //     AbsoluteRefreshTokenLifetime = 300,
                //     Claims = new List<ClientClaim>
                //     {
                //         new ClientClaim(JwtClaimTypes.Role, "admin")
                //     },
                // },
                new Client
                {
                    Enabled = true,
                    ClientId = "password.client",
                    ClientName = "MyBackend Client",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AccessTokenType = AccessTokenType.Jwt,
                    AllowedScopes = {
                        "MyBackendApi2",
                        IdentityServerConstants.StandardScopes.OpenId,
                    },
                    AlwaysSendClientClaims = true,
                    UpdateAccessTokenClaimsOnRefresh = true,
                    AlwaysIncludeUserClaimsInIdToken = true,
                    AllowAccessTokensViaBrowser = true,
                    IncludeJwtId = true,
                    ClientSecrets = { new Secret("secret".Sha256()) },
                    AllowOfflineAccess = true,
                    AccessTokenLifetime = 3600,

                    RefreshTokenUsage = TokenUsage.OneTimeOnly, // Or ReUse
                    RefreshTokenExpiration = TokenExpiration.Sliding,
                    AbsoluteRefreshTokenLifetime = 360000,
                    SlidingRefreshTokenLifetime = 36000,

                      ClientClaimsPrefix = string.Empty,
                    Claims = new List<ClientClaim> // Assign const roles 
                    {
                        new ClientClaim(JwtClaimTypes.Role, "admin"),
                        new ClientClaim(JwtClaimTypes.Role, "user")
                    }
                }
            };

        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "alice",
                    Password = "alice",
                    // Claims = new List<Claim>(){new Claim(JwtClaimTypes.Role,"admin") }
                },
                new TestUser
                {
                    SubjectId = "2",
                    Username = "bob",
                    Password = "password"
                }
            };
        }
    }
}